class Database{
    constructor(){

        this.loadFromFile = function(){}
        this.loadFromNetwork = function(){}

        this.loadJson = async function(tableName, jsonArr){
            return await nSQL().loadJS(tableName, jsonArr);
        }

        this.export = function(){

        }

        this.resetTable = function(){}
        this.loadAllTables = function(){}
        
        // specific to context
        this.configure = async function(){

            nSQL('client').model([ 
                {key:'identifiant',type:'int',props:['pk', 'ai']},
                {key:'nom',type:'string', default:"None"}, 
                {key:'prenom',type:'string', default:"None"}, 
                {key:'adresse',type:'string', default:"None"}, 
                {key:'codePostal',type:'string', default:"None"}, 
                {key:'ville',type:'string', default:"None"}, 
                {key:'telephone',type:'string', default:"None"}, 
                {key:'idCompteur',type:'string', default:"None"}, 
                {key:'ancienReleve',type:'float', default: 0},
                {key:'dateAncienReleve',type:'date'},
                {key:'dernierReleve',type:'float', default: 0},
                {key:'dateDernierReleve',type:'date'},
                {key:'signatureBase64',type:'string', default: 0},
                {key:'situation',type:'int', default: 0},
                {key:'idcontroleur',type:'string', default: 0},
            ])
            .config({
                mode: "PERM",
                history: true 
            }) 
            .views([ // Optional
                {
                    name: 'get_client',
                    args: ['identifiant:int'],
                    call: function(args, db) {
                        return db.query('select').where(['identifiant','=',args.identifiant]).exec();
                    }
                },
                {
                    name: 'list_all_client',
                    args: [],
                    call: function(args, db) {
                        return db.query('select').exec();
                    }
                }                       
            ]);

            nSQL('controleur').model([ 
                {key:'id',type:'string',props:['pk']},
                {key:'mp',type:'string', default:"None"}, 
            ])
            .config({
                mode: "PERM", 
                history: true 
            }) 
            .views([ 
                {
                    name: 'get_controleur',
                    args: ['id:string'],
                    call: function(args, db) {
                        return db.query('select').where(['id','=',args.id]).exec();
                    }
                }                       
            ]);

        }
        
        this.connect = async function(tableName){
            return nSQL(tableName).connect();
        }

        this.views = async function(tableName, name, args){
            return await nSQL(tableName).getView(name, args);
        }

        this.insert = async function(tableName, data){
            return await nSQL(tableName).query('upsert', data).exec();
        }

        this.select = async function(tableName, args){
            return await nSQL(tableName).query('select', data).exec();
        }

        this.select = async function(tableName){
            return await nSQL(tableName).query('select').exec();
        }

        this.selectWhere = async function(tableName, where){
            return await nSQL(tableName).query("select").where(where).exec();
        }

        this.delete = function(tableName, id){

        }

    }
}