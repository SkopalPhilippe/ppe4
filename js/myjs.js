async function loadClientHtml(domId){

    const db = new Database();
    db.configure();

    await db.connect("client");
    await db.loadJson("client", edf["client"]);
    let clients = await db.select("client");


    let result = [];
    for(const client of clients){
        // console.log(`href="/client/${client.identifiant}`);
        // alert(`href="/clientt/${client.identifiant}`);
        result.push(`<li> 
            <a href="/client/${client.identifiant}/" class="item-link item-content"> 
                <div class="item-inner">
                    <div class="item-title">
                        <div class="item-header">${client.identifiant}</div>
                        ${client.nom} ${client.prenom} ${client.telephone}
                        <div class="item-footer">${client.adresse} ${client.codePostal} ${client.ville} </div>
                    </div>
                </div>
            </a>
        </li>`);
    }

    // console.log(result);
    document.getElementById(domId).innerHTML = result;

    return true;
}