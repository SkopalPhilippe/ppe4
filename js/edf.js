var edf = {
    "controleur": [

        {"id": "bdurand","mp":"nYsNzGKzG4WuK5197n8h"},
        {"id": "hdubois","mp":"8FNbUUsUoYTNZAvEp3Jv"}

    ],"client": [

        {"identifiant":"1001","nom":"DUPONT","prenom":"Paul","adresse":"10 rue Anne Frank","codePostal":"49000","ville":"angers","telephone":"0624553212","idCompteur":"19950055123","ancienReleve":"1456.24","dateAncienReleve":"2012-03-15","dernierReleve":"0","dateDernierReleve":"0000-00-00","signatureBase64":"","situation":"0","idcontroleur":"bdurand"},
        {"identifiant":"1002","nom":"LULU","prenom":"Isabelle","adresse":"10 Avenue des arts et métiers","codePostal":"49000","ville":"angers","telephone":"0624553212","idCompteur":"19950055234","ancienReleve":"1546.22","dateAncienReleve":"2012-12-19","dernierReleve":"0","dateDernierReleve":"0000-00-00","signatureBase64":"","situation":"0","idcontroleur":"bdurand"},
        {"identifiant":"1003","nom":"CAOLIN","prenom":"Etienne","adresse":"10 rue Boisnet","codePostal":"49000","ville":"angers","telephone":"0624553212","idCompteur":"19990057894","ancienReleve":"45698.25","dateAncienReleve":"2013-02-04","dernierReleve":"0","dateDernierReleve":"0000-00-00","signatureBase64":"","situation":"0","idcontroleur":"bdurand"},
        {"identifiant":"1004","nom":"LEROY","prenom":"Alfred","adresse":"25 rue du quinconce","codePostal":"49000","ville":"angers","telephone":"0623553211","idCompteur":"20110055123","ancienReleve":"5656.26","dateAncienReleve":"2012-08-08","dernierReleve":"0","dateDernierReleve":"0000-00-00","signatureBase64":"","situation":"0","idcontroleur":"bdurand"},
        {"identifiant":"1005","nom":"CENTAURE","prenom":"Marie","adresse":"20 rue des lutins","codePostal":"49000","ville":"angers","telephone":"0744553212","idCompteur":"20070055123","ancienReleve":"57356.24","dateAncienReleve":"2012-05-15","dernierReleve":"0","dateDernierReleve":"0000-00-00","signatureBase64":"","situation":"0","idcontroleur":"hdubois"},
        {"identifiant":"1006","nom":"SALSA","prenom":"Solado","adresse":"12 rue locarno","codePostal":"49000","ville":"angers","telephone":"0612345678","idCompteur":"20080055123","ancienReleve":"8963.45","dateAncienReleve":"2012-11-13","dernierReleve":"0","dateDernierReleve":"0000-00-00","signatureBase64":"","situation":"0","idcontroleur":"hdubois"}

    ]
};
