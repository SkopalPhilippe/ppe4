routes = [
  {
    path: '/',
    url: './index.html',
  },
  {
    path: '/login/',
    url: './pages/login.html',
  },
  {
    path: '/releve/',
    url: './pages/releve.html',
  },
  {
    path: '/import/',
    url: './pages/import.html',
  },
  {
    path: '/export/',
    url: './pages/export.html',
  },
  {
    path: '/client/:clientId/',
    async: async function (routeTo, routeFrom, resolve, reject) {

      // User ID from request
      // console.log(routeTo);
      const clientId = routeTo.params.clientId;

      // console.log(this.data);
      // Router instance & app
      const router = this;
      const app = router.app;

      const db = new Database();
      db.configure();
  
      await db.connect("client");
      await db.loadJson("client", edf["client"]);

      let clientInfo = await db.views("client", "get_client", {identifiant: clientId});
      clientInfo = clientInfo[0];

      app.data["clientId"] = clientId;
      app.data["clientInfo"] = clientInfo;

      resolve({templateUrl: './pages/client.html'},
      {context: {
        clientId: clientId,
        identite: clientInfo.nom + " " + clientInfo.prenom,
        telephone: clientInfo.telephone,
        adresse: clientInfo.adresse + "\n" +  clientInfo.codePostal + " " + clientInfo.ville,
        compteur: clientInfo.idCompteur,
        ancienReleve: clientInfo.ancienReleve,
        oldDate: clientInfo.dateAncienReleve,
        releve: clientInfo.dernierReleve,
        date: clientInfo.dateDernierReleve,
        situation: clientInfo.situation
      }});

    }
      // Router instance
  },
  {
    path: '/signature/',
    url: './pages/signature.html',
  },
  // Page Loaders & Router
  {
    path: '/page-loader-template7/:user/:userId/:posts/:postId/',
    templateUrl: './pages/page-loader-template7.html',
  },
  {
    path: '/page-loader-component/:user/:userId/:posts/:postId/',
    componentUrl: './pages/page-loader-component.html',
  },
  {
    path: '/request-and-load/user/:userId/',
    async: function (routeTo, routeFrom, resolve, reject) {
      // Router instance
      var router = this;

      // App instance
      var app = router.app;

      // Show Preloader
      app.preloader.show();

      // User ID from request
      var userId = routeTo.params.userId;

      // Simulate Ajax Request
      setTimeout(function () {
        // We got user data from request
        var user = {
          firstName: 'Vladimir',
          lastName: 'Kharlampidi',
          about: 'Hello, i am creator of Framework7! Hope you like it!',
          links: [
            {
              title: 'Framework7 Website',
              url: 'http://framework7.io',
            },
            {
              title: 'Framework7 Forum',
              url: 'http://forum.framework7.io',
            },
          ]
        };
        // Hide Preloader
        app.preloader.hide();

        // Resolve route to load page
        resolve(
          {
            componentUrl: './pages/request-and-load.html',
          },
          {
            context: {
              user: user,
            }
          }
        );
      }, 1000);
    },
  },
  // Default route (404 page). MUST BE THE LAST
  {
    path: '(.*)',
    url: './pages/404.html',
  },
];
