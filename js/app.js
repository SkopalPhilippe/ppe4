// Dom7
var $$ = Dom7;

// Framework7 App main instance
var app  = new Framework7({
  root: '#app', // App root element
  id: 'io.framework7.testapp', // App bundle ID
  name: 'Framework7', // App name
  theme: 'auto', // Automatic theme detection
  // App root data
  data: function () {
    return {
      clientId: 0,
      clientInfo: {},
      signature64: ""
    };
  },
  // App root methods
  methods: {
    helloWorld: function () {
      app.dialog.alert('Hello World!');
    },
  },
  // App routes
  routes: routes,
  // Enable panel left visibility breakpoint
  panel: {
    leftBreakpoint: 960,
  },
  on: {
    init() {
      //device ready here
      console.log("Device ready");
    }
  }
});

// Init/Create left panel view
var leftView = app.views.create('.view-left', {
  url: '/'
});

// Init/Create main view
var mainView = app.views.create('.view-main', {
  url: '/'
});

// Login Screen Demo
$$('#my-login-screen .login-button').on('click', function () {
  var username = $$('#my-login-screen [name="username"]').val();
  var password = $$('#my-login-screen [name="password"]').val();

  // Close login screen
  app.loginScreen.close('#my-login-screen');

  // Alert username and password
  app.dialog.alert('Username: ' + username + '<br>Password: ' + password);
});

app.on('pageInit', function (page) {
  // do something on page init
  // app.dialog.alert("home");
  // app.dialog.alert(page.name);
  
});

// Chargement de releve
$$(document).on('page:init', '.page[data-name="releve"]',  function (e) {
  // Page Data contains all required information about loaded and initialized page
  var page = e.detail;
  // app.dialog.alert( page.name);

  loadClientHtml('clientList', 'test');

});

// Chargement de client
$$(document).on('page:init', '.page[data-name="client"]',  function (e) {

  
});

// Chargement de signature
$$(document).on('page:init', '.page[data-name="signature"]',  function (e) {
  initPaper();
});

// Dechargement de signature
$$(document).on('page:beforeout', '.page[data-name="signature"]',  function (e) {
  alert("leaving");
  // var canvas = document.getElementById('myCanvas');
  // canvas.on('onMouseDown', function(event){});
  // canvas.on('onMouseDrag', function(event){});
});


