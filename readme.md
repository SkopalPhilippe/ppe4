## Get started with cordova plugins

### debugger/console

To see console.log() outputs

* Installed by default, no npm needed
* enable deggub mode on phone ==> click on build version in system-info
* put adb.exe in the PATH environnement variable ==>
* my case: C:\Users\Philippe Skopal\AppData\Local\Android\Sdk\platform-tools
* install cordova plugon on visual studio
* get device id with : adb devices, configure it and then run it simply

### Speed up or slow machine 

* use cordova in browser 
* cordova platform add browser
* cordova run browser
* faster 

### Sqlite & ORM

* https://www.npmjs.com/package/cordova-plugin-nano-sqlite
* cordova plugin add cordova-plugin-nano-sqlite --save
* npm i cordova-plugin-nano-sqlite --save
*  <script src="https://cdn.jsdelivr.net/npm/nano-sql@1.6.3/dist/nano-sql.min.js"></script> 
* Put the above file in local
